Michael Chen
Ocean Optics 2021
Lab 5

This repo contains files for my analysis of data for Lab 5.

Sample analyzed: Harpswell Sound seawater, stored in an amber sample bottle. 
Lab group number: G2

Data files:
* in vivo fluorescence data from the WetLabs ECO chl a fluorometer:
	- G2_fluorometer_invivo.csv: concatenated, labeled data
	- G2_D1.TXT G2_D2.TXT, G2_D3.TXT, G2_D4.TXT, G2_D5.TXT, G2_D5.TXT G2_FSW.TXT, G4_DARK.TXT: raw datafiles.
	- Note, we did not collect a dark calibration, so we used another lab group's data
* in vitro fluorescence data from the Turner fluorometer (extracted chlorophyll):
	- G2_HS_amber_fluor_extracted.csv
	
